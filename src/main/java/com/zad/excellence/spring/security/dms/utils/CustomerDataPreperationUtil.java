package com.zad.excellence.spring.security.dms.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.zad.excellence.spring.security.dms.domain.Customer;

@Component
public class CustomerDataPreperationUtil {
	
	private List<Customer> customerList = new ArrayList<>();
	
	public CustomerDataPreperationUtil() {
		buildListOfCustomer();
	}
	
	public void buildListOfCustomer() {
		customerList.add(new Customer("C101","Saurav Naskar", "suarva@gmail.com"));
		customerList.add(new Customer("C102","Bijay Sinha","bsinha@gmail.com"));
		customerList.add(new Customer("C103","Santosh Burnwal","sburnwal@gmail.com"));		
	}
	
	public Optional<Customer> getCustomerById(String custId) {
		List<Customer> customers = this.getAllCustomers();
		return customers.stream().filter(cust -> cust.getCustomerId().equalsIgnoreCase(custId)).findFirst();
	}
	
	public List<Customer> getAllCustomers(){
		return this.customerList;
	}
	
	public boolean addNewCustomer(Customer customer) {
		return this.customerList.add(customer);
	}

}
