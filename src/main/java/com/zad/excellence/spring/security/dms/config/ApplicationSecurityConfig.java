package com.zad.excellence.spring.security.dms.config;

import static com.zad.excellence.spring.security.dms.constants.ApplicationUserPermission.CUSTOMER_WRITE;
import static com.zad.excellence.spring.security.dms.constants.ApplicationUserRole.ADMIN;
import static com.zad.excellence.spring.security.dms.constants.ApplicationUserRole.ADMINTRAINEE;
import static com.zad.excellence.spring.security.dms.constants.ApplicationUserRole.CUSROMER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import com.zad.excellence.spring.security.dms.constants.ApplicationUserRole;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private final PasswordEncoder passwordEncoder;
	
	@Autowired
	public ApplicationSecurityConfig(PasswordEncoder pwdEncoder) {
		this.passwordEncoder =pwdEncoder;
	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable()
		.authorizeRequests()
		.antMatchers("/springdemo/index","index","/CSS/*","/JS/*").permitAll()
		.antMatchers("/api/**").hasRole(ApplicationUserRole.CUSROMER.name())
		.antMatchers(HttpMethod.POST,"/management/api/**").hasAuthority(CUSTOMER_WRITE.getPermission())
		.antMatchers(HttpMethod.PUT,"/management/api/**").hasAuthority(CUSTOMER_WRITE.getPermission())
		.antMatchers(HttpMethod.DELETE,"/management/api/**").hasAuthority(CUSTOMER_WRITE.getPermission())
		.antMatchers(HttpMethod.GET,"/management/api/**").hasAnyRole(ApplicationUserRole.ADMIN.name(), ApplicationUserRole.ADMINTRAINEE.name())
		.anyRequest()
		.authenticated()
		.and()
		.httpBasic();
	}
	
	@Override
	@Bean
	protected UserDetailsService userDetailsService() {
		UserDetails rsmithUser = User.builder()
				.username("rsmith")
				.password(passwordEncoder.encode("password"))
				.roles(CUSROMER.name())
				.authorities(CUSROMER.getGrantedAuthorities())
				.build();
		
		UserDetails kzahidUser = User.builder()
				.username("kzahid")
				.password(passwordEncoder.encode("Fight@2020"))
				.roles(ADMIN.name())
				.authorities(ADMIN.getGrantedAuthorities())
				.build();
		
		UserDetails mclarkUser = User.builder()
				.username("mclark")
				.password(passwordEncoder.encode("password@2020"))
				.roles(ADMINTRAINEE.name())
				.authorities(ADMINTRAINEE.getGrantedAuthorities())
				.build();
		
		return new InMemoryUserDetailsManager(rsmithUser,kzahidUser,mclarkUser);
	}

}
