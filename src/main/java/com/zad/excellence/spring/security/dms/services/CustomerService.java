package com.zad.excellence.spring.security.dms.services;

import java.util.Collection;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.zad.excellence.spring.security.dms.domain.Customer;

@Service
public interface CustomerService {
	
	public Collection<Customer> getAllCustomers();
	public Optional<Customer> getCustomerById(String customerId);
	public boolean addNewCustomer(Customer customer);

}
