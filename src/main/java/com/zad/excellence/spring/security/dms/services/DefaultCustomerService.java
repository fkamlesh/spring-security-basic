package com.zad.excellence.spring.security.dms.services;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zad.excellence.spring.security.dms.domain.Customer;
import com.zad.excellence.spring.security.dms.utils.CustomerDataPreperationUtil;

@Service
public class DefaultCustomerService implements CustomerService {

	@Autowired
	private CustomerDataPreperationUtil customerDataPreperationUtil;

	@Override
	public Collection<Customer> getAllCustomers() {
		return customerDataPreperationUtil.getAllCustomers();
	}

	@Override
	public Optional<Customer> getCustomerById(String customerId) {

		return customerDataPreperationUtil.getCustomerById(customerId);
	}

	@Override
	public boolean addNewCustomer(Customer customer) {
		return customerDataPreperationUtil.addNewCustomer(customer);
	}

}
