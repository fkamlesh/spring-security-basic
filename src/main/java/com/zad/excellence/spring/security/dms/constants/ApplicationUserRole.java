package com.zad.excellence.spring.security.dms.constants;

import static com.zad.excellence.spring.security.dms.constants.ApplicationUserPermission.CUSTOMER_READ;
import static com.zad.excellence.spring.security.dms.constants.ApplicationUserPermission.CUSTOMER_WRITE;
import static com.zad.excellence.spring.security.dms.constants.ApplicationUserPermission.PRODUCT_READ;
import static com.zad.excellence.spring.security.dms.constants.ApplicationUserPermission.PRODUCT_WRITE;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.collect.Sets;

public enum ApplicationUserRole {
	
	CUSROMER(Sets.newHashSet()),
	ADMIN(Sets.newHashSet(CUSTOMER_READ,CUSTOMER_WRITE,PRODUCT_READ, PRODUCT_WRITE)),
	ADMINTRAINEE(Sets.newHashSet(CUSTOMER_READ,PRODUCT_READ));
	
	private final Set<ApplicationUserPermission> permissions;
	
	ApplicationUserRole(Set<ApplicationUserPermission> permissions){
		this.permissions = permissions;
	}
	
	public Set<ApplicationUserPermission> getPermissions() {
		return this.permissions;
	}
	
	public Set<SimpleGrantedAuthority> getGrantedAuthorities(){
		Set<SimpleGrantedAuthority> permissions = 
				getPermissions().stream().map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
				.collect(Collectors.toSet());
		permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
		return permissions;
	}

}
