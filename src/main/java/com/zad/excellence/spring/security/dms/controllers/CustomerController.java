package com.zad.excellence.spring.security.dms.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zad.excellence.spring.security.dms.domain.Customer;
import com.zad.excellence.spring.security.dms.services.DefaultCustomerService;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {
	
	@Autowired
	private DefaultCustomerService customerService;
	
	@GetMapping("/{customerId}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable("customerId") String customerId ){
		Optional<Customer> customer = customerService.getCustomerById(customerId);
		return customer.isPresent() ?  new ResponseEntity<>(customer.get(), HttpStatus.OK) : 
			new ResponseEntity<>(new Customer(), HttpStatus.NOT_FOUND);		
	}

}
