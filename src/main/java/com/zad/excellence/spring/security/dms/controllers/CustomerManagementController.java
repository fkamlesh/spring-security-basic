package com.zad.excellence.spring.security.dms.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zad.excellence.spring.security.dms.domain.Customer;
import com.zad.excellence.spring.security.dms.services.DefaultCustomerService;

@RestController
@RequestMapping("management/api/v1/customers")
public class CustomerManagementController {
	
	@Autowired
	private DefaultCustomerService customerService;

	@GetMapping
	public ResponseEntity<Collection<Customer>> getAllCustomers(){
		Collection<Customer> customers = customerService.getAllCustomers();
		return new ResponseEntity<>(customers, HttpStatus.OK);		
	}
	
	@PostMapping
	public ResponseEntity<String> addNewCustomer(@RequestBody Customer customer ){
		return customerService.addNewCustomer(customer) ? 
				new ResponseEntity<>("success", HttpStatus.CREATED) :
					new ResponseEntity<>("Failed", HttpStatus.METHOD_FAILURE);					
	}
}
